import os, time, pd, shutil
from datetime import *

source_folder = "HERE YOU MUST PUT THE FROM"
destiny_folder = "HERE YOU MUST PUT THE DESTINY"

########################################################
## This will create a critical tima that is 2h limit. ##
########################################################
criticalTime = datetime.now()+timedelta(hours=-2)

##############################
## Scan the directory       ##
##############################

obj = os.scandir(source_folder)

##############################################################
## Check and save it in order to filter the content         ##
##############################################################

for entry in obj:

    #######################################################
    ## Move only files and diretory (ALTER IT IF NEEDED) ##
    #######################################################

    if entry.is_dir() or entry.is_file():
    
       ######################################
       ## Saving file/directory datetime   ##
       ######################################

       modified_time = os.path.getmtime(source_folder+entry.name)
       dlter=datetime.fromtimestamp(modified_time)

        if dlter < criticalTime:
           
           ########################################################
           ## Giving the value to the variables in order to move ##
           ########################################################

           original=source_folder+entry.name
           target=destiny_folder+entry.name

           ####################################
           ## MOVING THE FILES / DIRECTORIES ##
           ####################################

           shutil.move(original, target)
