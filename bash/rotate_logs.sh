#!/bin/bash

# Directory where you will locate the logs
log_dir="/var/log/"

# Find all the files in the directory with the extension LOG
for file in $(find $log_dir -name "*.log"); do
  
  # Obtain the actual date with the format YYYY-MM-DD
  date_string=$(date +%F)

  # Rename the file
  mv $file $file.$date_string

  # Create the same file but empty
  touch $file
done

# Find all the files in the directory with the extension LOG
for file in $(find $log_dir -name "*.log" -mtime +2); do

  # Compress the file with gzip
  gzip $file
done
